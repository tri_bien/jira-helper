package com.example.demo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JiraHelperApplication {

  private String driverPath = "/home/tri/chromedriver_linux64/chromedriver";

  private WebDriver driver;
  private WebDriverWait interactableWait;
  private Wait<WebDriver> fluentWait;

  private String userStory;
  private List<String> taskNames;
  private String issueType = "Sub-task";
  private String priority = "Prio04";
  private String label = "Java_Accounting_Console";
  private String component = "Accounting Console";

  public static void main(String[] args)
      throws URISyntaxException, IOException, InterruptedException {
    JiraHelperApplication jiraHelper = new JiraHelperApplication();

    String taskFilename = "tasks.txt";
    jiraHelper.createSubTasks(taskFilename);
    jiraHelper.close();
  }

  private void init() {
    System.setProperty("webdriver.chrome.driver", driverPath);

    ChromeOptions options = new ChromeOptions();
    options.addArguments("start-maximized");
//    options.addArguments("--auto-open-devtools-for-tabs");

    driver = new ChromeDriver(options);
    interactableWait = new WebDriverWait(driver, 60);
    fluentWait = new FluentWait<>(driver)
        .withTimeout(Duration.of(300, ChronoUnit.SECONDS))
        .pollingEvery(Duration.of(1, ChronoUnit.SECONDS))
        .ignoring(Exception.class);
  }

  private void goToLoginPage() {
    driver.get("https://tickets.unifiedpost.com/secure/RapidBoard.jspa");
    driver.manage().window().maximize();
  }

  private void clickOnSelectedUserStory() {
    WebElement userStoryElement = fluentWait.until(new Function<WebDriver, WebElement>() {
      public WebElement apply(WebDriver driver) {
        String cssUserStory = String.format("a[href='/browse/%s']", userStory);
        return driver.findElement(By.cssSelector(cssUserStory));
      }
    });

    // click on requested Story
    userStoryElement.click();
  }

  private void clickOnMoreAction() throws Exception {
    WebElement moreActionElement = fluentWait.until(new Function<WebDriver, WebElement>() {
      public WebElement apply(WebDriver driver) {
        return driver.findElement(
            By.cssSelector("button.aui-button.ghx-actions.aui-button-compact.aui-button-subtle"));
      }
    });
    moreActionElement.click();
  }

  private void tryToClickOnMoreAction(int loop, int sleepMilliseconds)
      throws InterruptedException {
    for (int i = 0; i < loop; i++) {
      try {
        reloadPage();
        clickOnMoreAction();
        break;
      } catch (Exception e) {
        Thread.sleep(sleepMilliseconds);
      }
    }
  }

  private void clickOnCreateSubtaskToOpenForm() throws InterruptedException {
    Thread.sleep(1000);

    By createSubTaskLocator =
        By.xpath("//a[text()='Create sub-task']");
    WebElement createSubTaskElement = driver.findElement(createSubTaskLocator);
    waitUntilInteractable(interactableWait, createSubTaskElement, createSubTaskLocator);

    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].click();", createSubTaskElement);
  }

  private void reloadPage() {
    driver.navigate().refresh();
  }

  public void createSubTasks(String taskFilename)
      throws URISyntaxException, IOException, InterruptedException {
    getTasksFromFile(taskFilename);

    init();

    goToLoginPage();

    collapseAllUserStories();

    expandOtherIssues();

    for (String taskName : taskNames) {

      // reload page to get DOM elements
      reloadPage();

      clickOnSelectedUserStory();

      tryToClickOnMoreAction(2, 3000);

      clickOnCreateSubtaskToOpenForm();

      fillInfoInCreateSubTaskForm(taskName);
    }
  }

  private void expandOtherIssues() {
    try {
      // no need to wait a try here
      driver.findElement(
          By.cssSelector("div.ghx-swimlane-header.ghx-swimlane-default")).click();
    } catch (Exception e) {
      System.out.println("No need to click on Other Issues in this case");
    }
  }

  private void clickOnBoardButton() {
    WebElement boardButton = fluentWait.until(new Function<WebDriver, WebElement>() {
      public WebElement apply(WebDriver driver) {
        return driver.findElement(
            By.cssSelector("button#board-tools-section-button"));
      }
    });
    boardButton.click();
  }

  private void collapseAllUserStories() {
    clickOnBoardButton();

    clickOnCollapseButton();
  }

  private void clickOnCollapseButton() {
    WebElement collapseButton = fluentWait.until(new Function<WebDriver, WebElement>() {
      public WebElement apply(WebDriver driver) {
        return driver.findElement(
            By.cssSelector("a.js-view-action-collapse-all"));
      }
    });
    collapseButton.click();
  }

  private void fillInfoInCreateSubTaskForm(String taskSummary) throws InterruptedException {
    selectIssueTypeSubTask();

    waitAfterSelectIssueType();

    tryToFillTaskSummary(taskSummary, 3, 3000);

    fillPriority();

    fillComponent();

    // scroll down
    scrollVertical(-1400);

    fillLabel();

    submitToCreateSubTask();
  }

  private void close() {
    driver.close();
  }

  private void submitToCreateSubTask() {
    driver.findElement(By.id("create-issue-submit")).click();
  }

  private void fillLabel() {
    WebElement labelElement = driver.findElement(By.id("labels-textarea"));
    labelElement.click();
    labelElement.sendKeys(label);
    labelElement.sendKeys(Keys.TAB);
  }

  private void fillComponent() {
    Select componentElement = new Select(driver.findElement(By.id("components")));
    componentElement.selectByVisibleText(component);
  }

  private void fillPriority() {
    driver.findElement(By.cssSelector("div#priority-single-select")).click();
    WebElement priorityElement = driver.findElement(By.cssSelector("input#priority-field"));
    priorityElement.sendKeys(priority);
    priorityElement.sendKeys(Keys.TAB);
  }

  private void waitAfterSelectIssueType() {
    // wait until button Create is clickable
    By createLocator = By.id("create-issue-submit");
    WebElement createElement = fluentWait.until(new Function<WebDriver, WebElement>() {
      public WebElement apply(WebDriver driver) {
        return driver.findElement(createLocator);
      }
    });
    waitUntilInteractable(interactableWait, createElement, createLocator);
  }

  private void selectIssueTypeSubTask() {
    WebElement issueTypeElement = fluentWait.until(new Function<WebDriver, WebElement>() {
      public WebElement apply(WebDriver driver) {
        return driver.findElement(By.cssSelector("input#issuetype-field"));
      }
    });
    issueTypeElement.click();
    issueTypeElement.sendKeys(issueType);
  }

  private void tryToFillTaskSummary(String taskSummary, int loop, int sleepMilliseconds)
      throws InterruptedException {
    for (int i = 0; i < loop; i++) {
      try {
        fillTaskSummary(taskSummary);
        break;
      } catch (ElementNotInteractableException e) {
        Thread.sleep(sleepMilliseconds);
      }
    }
  }

  private void fillTaskSummary(String taskSummary) {
    By summaryLocator = By.cssSelector("input#summary");
    WebElement summaryElement = fluentWait.until(new Function<WebDriver, WebElement>() {
      public WebElement apply(WebDriver driver) {
        return driver.findElement(summaryLocator);
      }
    });
    waitUntilInteractable(interactableWait, summaryElement, summaryLocator);
    summaryElement.click();
    summaryElement.sendKeys(taskSummary);
  }

  private void scrollVertical(int pixel) {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    String command = String.format("window.scrollBy(0,%s)", pixel);
    js.executeScript(command, "");
  }

  private void waitUntilInteractable(WebDriverWait waiter, WebElement element, By locator) {
    waiter.until(ExpectedConditions.presenceOfElementLocated(locator));
    waiter.until(ExpectedConditions.visibilityOf(element));
    waiter.until(ExpectedConditions.elementToBeClickable(element));
  }

  private void getTasksFromFile(String filename) throws URISyntaxException, IOException {
    Path path = Paths.get(this.getClass().getClassLoader().getResource(filename).toURI());
    List<String> tasks = Files.readAllLines(path, Charset.defaultCharset());
    this.userStory = tasks.get(0);
    this.taskNames = tasks.subList(1, tasks.size());
  }
}
